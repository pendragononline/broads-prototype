//Hide and show elements

$(document).ready(function () {

    //Hide by default
    $('#pg1').css('display', 'none');
    $('#pg2').css('display', 'none');
    $('#pg3').css('display', 'none');
    $('#pg4').css('display', 'none');
    $('.info-button').css('display', 'none');

    //Stack
    var stack = 0;

    //Global events
    //Dragonfly logo click
    $('#dragonfly').click(function (e) {
        //Hide all windows apart from the landing page
        $('.pg:not(#pg1)').slideUp();
        $('#pg1').css('display', 'block');
        stack = 1;
    });

    //Ecology button tap
    $('#ecology').click(function (e) {
        location.href = 'hidden-habitats/carlton_marsh_intro.html';
    });

    //Info button tap
    $('.info-button').click(function (e) {
        $('.pg:not(#pg4)').slideUp();
        $('#pg4').css('display', 'block');
    });

    //Info button return
    $('#close-info').click(function (e) {
        $('#pg4').slideUp();
        $('#pg'+stack.toString()).css('display', 'block');
    });

    //--PROGRAM--//

    //When the user taps on 'Next' on the landing page
    $('#start').click(function (e) {
        $('.pg:not(#pg1)').slideUp();
        $('#pg1').css('display', 'block');
        $('.info-button').css('display', 'block');
        stack = 1;
    });

    //When the user taps on 'Cultural' on the landing page
    $('#cultural').click(function (e) {
        $('.pg:not(#pg2)').slideUp();
        $('#pg2').css('display', 'block');
        stack = 2;
    });

    //When the user taps on 'Next'
    $('#next').click(function (e) {
        $('.pg:not(#pg3)').slideUp();
        $('#pg3').css('display', 'block');
        stack = 3;
    });

    //When the user taps on 'Previous on experience landing page'
    $('#prev').click(function (e) {
        $('#pg2').slideUp();
        stack = stack - 1;
        $('#pg' + stack.toString()).css('display', 'block');
    });

    //When the user exits the experience
    $('#close-exp').click(function (e) {
        $('#pg3').slideUp();
        $('#pg1').css('display', 'block');
    });

});
